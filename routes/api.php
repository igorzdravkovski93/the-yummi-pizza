<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/menu', 'Api\ProductsController@getProducts');
Route::get('/menu/top_products', 'Api\ProductsController@topProducts');
Route::get('/menu/newest_products', 'Api\ProductsController@newestProducts');
Route::get('/product/{id}', 'Api\ProductsController@getProduct');
Route::post('/processPayment', 'Api\PaymentController@handlePayment');
Route::post('/login', 'Api\UsersController@login');
Route::get('/transactions', 'Api\DashboardController@getTransactions');
Route::get('/dashboardData', 'Api\DashboardController@getWidgetData');