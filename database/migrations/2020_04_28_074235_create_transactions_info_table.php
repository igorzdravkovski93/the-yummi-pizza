<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_name');
            $table->string('customer_address');
            $table->string('customer_phone');
            $table->decimal('products_total_price_euro', 8, 2);
            $table->decimal('products_total_price_dollar', 8, 2);
            $table->decimal('delivery_cost_euro', 8, 2);
            $table->decimal('delivery_cost_dollar', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyers_info');
    }
}
