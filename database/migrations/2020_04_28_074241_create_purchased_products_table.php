<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchased_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('transaction_id')->unsigned();
            $table->integer('quantity');
            $table->timestamps();
            $table->foreign('product_id', 'purchases_products')
                  ->references('id')
                  ->on('products');
            $table->foreign('transaction_id', 'transactions_products')
                  ->references('id')
                  ->on('transactions_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchased_products');
    }
}
