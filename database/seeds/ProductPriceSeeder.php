<?php

use Illuminate\Database\Seeder;
use App\ProductPrice;

class ProductPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i < 10; $i++) {
            $price = rand (5.2*10, 12.3*10) / 10;
            ProductPrice::create([
                'euro' => $price,
                'dollar' => $price * 1.09,
                'product_id' => $i,
            ]);
        }
    }
}
