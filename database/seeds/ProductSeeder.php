<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            ['title' => 'REEF, STEAK & BACON', 'description' => 'Juicy prawns, seasoned steak & crispy rasher bacon, topped with creamy hollandaise sauce & spring onions'],
            ['title' => 'GARLIC CHICKEN & BACON RANCH', 'description' => 'Succulent chicken, crispy rasher bacon, spinach and red onion, topped with a creamy ranch sauce and served on a pizza sauce base with zesty garlic sauce.
            '],
            ['title' => 'CHICKEN, BACON & AVOCADO', 'description' => 'Succulent seasoned chicken, creamy avocado, crispy rasher bacon & red onion, topped with hollandaise sauce & spring onions'],
            ['title' => 'LOADED SUPREME', 'description' => 'Ground beef, crispy rasher bacon, mushroom, pepperoni, Italian sausage, fresh baby spinach, smoked leg ham, pineapple, topped with oregano, tomato capsicum sauce & spring onion.'],
            ['title' => 'MEGA MEATLOVERS', 'description' => 'Mega loaded, mega tasty. Featuring seasoned chicken, pork & fennel sausage, crumbled beef, pepperoni slices, Italian sausage & crispy rasher bacon, brought together with a Hickory BBQ sauce'],
            ['title' => 'CHICKEN & CAMEMBERT', 'description' => 'Succulent chicken, melted camembert, crispy rasher bacon, Italian cherry tomatoes, baby spinach & sliced red onion, topped with hollandaise sauce'],
            ['title' => 'PERI PERI CHICKEN', 'description' => 'A flavoursome pairing of seasoned chicken pieces, Italian cherry tomatoes, sliced red onion & baby spinach, topped with a swirl of our addictive peri peri sauce'],
            ['title' => 'BBQ CHICKEN & RASHER BACON', 'description' => 'The perfect combination of succulent chicken pieces, crispy rasher bacon & slices of red onion on a BBQ sauce base'],
            ['title' => 'GARLIC PRAWN', 'description' => 'Juicy prawns, paired with fresh baby spinach & diced tomato on a crème fraiche & zesty garlic sauce base, topped with oregano'],
        ];

        foreach($products as $product) {
            Product::create([
                'title' => $product['title'],
                'description' => $product['description'],
                'total_amount_of_purchases' => rand(5,95),
            ]);
        }
    }
}
