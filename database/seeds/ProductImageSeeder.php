<?php

use Illuminate\Database\Seeder;
use App\ProductImage;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i < 10; $i++) {
            $imageSmall = url('/')."/images/products/$i/$i.jpg";
            $imageLarge = url('/')."/images/products/$i/$i.$i.jpg";

            ProductImage::create([
                'product_id' => $i,
                'img_path' => $imageSmall
            ]);

            ProductImage::create([
                'product_id' => $i,
                'img_path' => $imageLarge
            ]);
        }
    }
}
