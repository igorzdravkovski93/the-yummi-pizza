<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchasedProduct extends Model
{
    protected $table = 'purchased_products';
    protected $guarded = [];

    public function transaction() {
        return $this->belongsTo(TransactionInfo::class, 'transaction_id');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
