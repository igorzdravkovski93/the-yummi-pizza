<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ProductImage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProducts()
    {
        $products = Product::get();
        $data = [];
        foreach($products as $product) {

            $productImages = ProductImage::where('product_id',$product->id)->get();
            $images = [];
            foreach($productImages as $image) {
                $images[] = $image->img_path;
            }

            $price = $product->price;
            
            $data[] = [
                'product' => $product,
                'image' => $images[0],
                'price_euro' => $price->euro,
                'price_dollar' => $price->dollar,
            ];
        }
        // dd($data);
        return response()->json(['success' => 1, 'data' => $data]);
    }

    public function getProduct($id)
    {
        $product = Product::find($id);
        $data = [];
        $productImages = ProductImage::where('product_id',$product->id)->get();
        $images = [];
        foreach($productImages as $image) {
            $images[] = $image->img_path;
        }

        $price = $product->price;
        
        $data[] = [
            'product' => $product,
            'image' => $images[1],
            'price_euro' => $price->euro,
            'price_dollar' => $price->dollar,
        ];
        
        return response()->json(['success' => 1, 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function topProducts()
    {
        $products = Product::orderBy('total_amount_of_purchases', 'desc')->limit(4)->get();
        $data = [];
        foreach($products as $product) {
            
            $productImages = ProductImage::where('product_id',$product->id)->get();
            $images = [];
            foreach($productImages as $image) {
                $images[] = $image->img_path;
            }

            $price = $product->price;
            
            $data[] = [
                'product' => $product,
                'image' => $images[1],
            ];
        }

        return response()->json(['success' => 1, 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function newestProducts()
    {
        $products = Product::orderBy('id', 'desc')->limit(4)->get();

        $data = [];
        foreach($products as $product) {
            
            $productImages = ProductImage::where('product_id',$product->id)->get();
            $images = [];
            foreach($productImages as $image) {
                $images[] = $image->img_path;
            }

            $price = $product->price;
            
            $data[] = [
                'product' => $product,
                'image' => $images[1],
            ];
        }

        return response()->json(['success' => 1, 'data' => $data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
