<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TransactionInfo;
use App\Product;
use App\PurchasedProduct;
use App\ProductPrice;

class DashboardController extends Controller
{
    public function getTransactions(Request $request) {
        $transactions = TransactionInfo::orderBy('id', 'desc')->get();
        
        if($transactions) {
            $data = [];
            foreach($transactions as $transaction) {
                $transactionProducts = PurchasedProduct::where('transaction_id', $transaction->id)->get();

                $transactionData = [
                    'transaction' => $transaction,
                ];
                
                foreach($transactionProducts as $transactionProduct) {
                    $product = Product::find($transactionProduct->id);
                    
                    $transactionData['products'][] = [
                        'product' => $product, 
                        'quantity' => $transactionProduct->quantity,
                        'product_prices' => [
                            'euro' => $product->price->euro,
                            'dollar' => $product->price->dollar
                        ],
                        'image' => $product->image->first()
                    ];
                }
                $data[] = $transactionData;
            }
            
            
            return response()->json([
                'success' => 1,
                'data' => $data
            ]);
        }

        return response()->json([
            'error' => 1
        ]);
    } 

    public function getWidgetData(Request $request) {
        $transactions = TransactionInfo::get();
        $totalIncomeEuro = 0;
        $totalIncomeDollar = 0;

        foreach($transactions as $transaction) {
            $totalIncomeEuro += $transaction->products_total_price_euro;
            $totalIncomeEuro += $transaction->delivery_cost_euro;
            $totalIncomeDollar += $transaction->products_total_price_dollar;
            $totalIncomeDollar += $transaction->delivery_cost_dollar;
        }

        $products = Product::get();

        $numberOfPurchasedProducts = 0;
        $purchasedProducts = PurchasedProduct::get();

        foreach($purchasedProducts as $product) {
            $numberOfPurchasedProducts += $product->quantity;
        }


        $data = [
            'number_of_transactions' => count($transactions),
            'total_income_euro' => $totalIncomeEuro,
            'total_income_dollar' => $totalIncomeDollar,
            'number_of_products' => count($products),
            'number_of_sold_products' => $numberOfPurchasedProducts
        ];

        return response()->json([
            'success' => 1,
            'data' => $data
        ]);
    }
}
