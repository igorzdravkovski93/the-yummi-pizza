<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public function login(Request $request) {

        $user = User::select('email', 'password')
                    ->where('email', $request->email)
                    ->first();

            
        if($user) {
            if(\Hash::check($request->password, $user->password)) {
                return response()->json([
                    'success' => 1,
                ]);
            }
        }

        return response()->json([
            'error' => 1,
            'msg' => 'Wrong credentials!'
        ]);

    }
}
