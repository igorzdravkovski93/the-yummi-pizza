<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TransactionInfo;
use App\PurchasedProduct;
use App\Product;

class PaymentController extends Controller
{
    public function handlePayment(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:7',
        ]);

        if($validator->fails()) {
            return response()->json([
                'error' => 1,
                'messages' => $validator->messages()
            ]);
        }

        $transaction = TransactionInfo::create([
            'customer_name' => $request->name,
            'customer_address' => $request->address,
            'customer_phone' => $request->phone,
            'products_total_price_euro' => $request->products_price['euro'],
            'products_total_price_dollar' => $request->products_price['dollar'],
            'delivery_cost_euro' =>$request->delivery_costs['euro'],
            'delivery_cost_dollar' => $request->delivery_costs['dollar'],
        ]);

        foreach($request->products as $product) {
            PurchasedProduct::create([
                'product_id' => $product['product_id'],
                'quantity' => $product['quantity'],
                'transaction_id' => $transaction->id
            ]);

            $currentProduct = Product::find($product['product_id']);
            $currentProduct->total_amount_of_purchases += $product['quantity'];
            $currentProduct->updated_at = date('Y-m-d H:i:s');
            $currentProduct->update();
        } 

        return response()->json([
            'success' => 1,
            'messageTitle' => 'Payment successful',
            'message' => 'Thank you for your order.'
        ]);

        
    }
}
