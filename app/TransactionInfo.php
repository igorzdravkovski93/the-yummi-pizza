<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionInfo extends Model
{
    protected $table = 'transactions_info';
    protected $guarded = [];

    public function purchased_product() {
        return $this->hasMany(PurchasedProduct::class);
    }
}
