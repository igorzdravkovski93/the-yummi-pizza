<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = [];

    public function image() {
        return $this->hasMany(ProductImage::class);
    }

    public function price() {
        return $this->hasOne(ProductPrice::class);
    }

    public function purchased_product() {
        return $this->hasMany(PurchasedProduct::class);
    }
}
