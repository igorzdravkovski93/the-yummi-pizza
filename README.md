# The Yummi Pizza

This is a demo pizza shop app

# This app is build with:

* Frontend: React.js
* Backend: Laravel

### Running this app requires php, mysql, npm and node.js installed on your local machine

# Step 1: Clone app from this repo:

```
$ git clone https://gitlab.com/igorzdravkovski93/the-yummi-pizza.git
```

# Step 2: Run this commands bellow to set the Laravel enviroment:

```
$ cd the-yummi-pizza/
```
```
$ composer install
```
```
$ cp .env.example .env
```
```
$ php artisan key:generate
```

### Go to mysql and create database, and after that edit .env file:

APP_URL=http://localhost:8000

DB_DATABASE=[your database name]

DB_USERNAME=[your mysql username]

DB_PASSWORD=[your mysql password]


### Run this commands to create your database:

```
$ php artisan migrate
```

```
$ php artisan db:seed
```

# Step 3: Run this commands bellow to install React dependencies:

```
$ npm install
```

# Step 4: Start your local server:

```
$ php artisan serve
```

# Step 5: Open your browser and go to http://localhost:8000/

