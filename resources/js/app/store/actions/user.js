export const login = (data) => {
    return {
        type: 'LOG_IN',
        provider: data
    }
}

export const logout = () => {
    return {
        type: 'LOG_OUT'
    }
}
