export const addProduct = (data) => {
    return {
        type: 'ADD_TO_CART',
        provider: data
    }
}

export const modifyProduct = (data) => {
    return {
        type: 'MODIFY_PRODUCT',
        provider: data
    }
}

export const emptyCard = () => {
    return {
        type: 'EMPTY_CARD',
    }
}