export const euro = () => {
    return {
        type: 'EURO'
    }
}

export const dollar = () => {
    return {
        type: 'DOLLAR'
    }
}
