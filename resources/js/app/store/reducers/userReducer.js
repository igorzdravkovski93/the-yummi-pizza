const userReducer = (state = '', action) => {
    switch(action.type) {
        case 'LOG_IN':
            return action.provider;
        case 'LOG_OUT':
            return state = '';
        default: 
            return state;
    }
}

export default userReducer;