const currencyReducer = (state = 'euro', action) => {
    switch(action.type) {
        case 'EURO':
            return state = 'euro';
        case 'DOLLAR':
            return state = 'dollar';
        default: 
            return state;
    }
}

export default currencyReducer;