// const INITIAL_STATE = () => {
//     let getShoppingCart = JSON.parse(localStorage.getItem('persist:root'))
//     return getShoppingCart.shoppingCart
// }

let getShoppingCart = JSON.parse(localStorage.getItem('persist:root'))
let INITIAL_STATE = ''
if(!getShoppingCart || typeof getShoppingCart.shoppingCart === 'undefined') {
    INITIAL_STATE = []
} else {
    INITIAL_STATE = getShoppingCart.shoppingCart
}



const shoppingCartReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'ADD_TO_CART':
            return state = [...state, action.provider];
        case 'MODIFY_PRODUCT':
            return state = action.provider;
        case 'EMPTY_CARD':
            return state = []
        default: 
            return state;
    }
}

export default shoppingCartReducer;