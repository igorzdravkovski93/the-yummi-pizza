import currencyReducer from './currencyReducer';
import shoppingCartReducer from './shoppingCartReducer';
import userReducer from './userReducer';
import { combineReducers } from 'redux';

const allReducers = combineReducers({
    currency: currencyReducer,
    shoppingCart: shoppingCartReducer,
    user: userReducer
})

export default allReducers;