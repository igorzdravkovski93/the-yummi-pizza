import React, { Suspense } from 'react';
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import storage from 'redux-persist/lib/storage'
import allReducers from './store//reducers/index'
import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";

import { Row, Col } from 'reactstrap'
import Notifications from 'react-notify-toast'

import Header from './components/Header'
import Footer from './components/Footer'
import Home from './views/Home'
import Menu from './views/Menu'
import MenuItem from './views/MenuItem'
import ShoppingCart from './views/ShoppingCart';
import Login from './views/Login';
import Dashboard from './views/Dashboard';

const persistConfig = {
    key: 'root',
    storage,
  }
   
const persistedReducer = persistReducer(persistConfig, allReducers)

const store = createStore(
    persistedReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()    
);
const persistor = persistStore(store)

const loading = () => <div className="animated fadeIn pt-1 text-center text-white">Loading...</div>

const App = () =>  {
    return (
        <Router>
            <Notifications />
            <Row>
                <Col xs="12" style={{padding: 0}}>
                    <Header/>
                </Col>
            </Row> 
            <Suspense fallback={() => loading()}>
                <Switch>
                    <Route exact path="/menu/:id" component={MenuItem} />
                    <Route exact path="/menu" component={Menu} />
                    <Route exact path="/checkout" component={ShoppingCart} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/dashboard" component={Dashboard} />
                    <Route path="/" component={Home} />
                </Switch>
            </Suspense>
            <Row>
                <Col xs="12" style={{padding: 0}}>
                    <Footer/>
                </Col>
            </Row> 
        </Router>
    )
}

export default App;

if (document.getElementById('root')) {
    ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>, 
    document.getElementById('root'));
}
