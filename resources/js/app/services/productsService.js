const axios = require('axios').default;

class productsService {

  static getProducts () {
    return axios.get('/api/menu')
  }

  static getCurrentProduct (id) {
    return axios.get(`/api/product/${id}`)
  }

  static getTopProducts () {
    return axios.get('/api/menu/top_products')
  }

  static getNewestProducts () {
    return axios.get('/api/menu/newest_products')
  }
}

export default productsService;