const axios = require('axios').default;

class productsService {

  static processPayment (data) {
    return axios.post('/api/processPayment', data)
  }
}

export default productsService;