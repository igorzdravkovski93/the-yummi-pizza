const axios = require('axios').default;

class dashboardService {

  static getTransactions (data) {
    return axios.get('/api/transactions', data)
  }

  static getWidgetData (data) {
    return axios.get('/api/dashboardData', data)
  }
}

export default dashboardService;