const axios = require('axios').default;

class userService {

  static login (data) {
    return axios.post('/api/login', data)
  }
}

export default userService;