import React, { useState, useEffect } from 'react';
import {
    Form,
    FormGroup,
    Label,
    Input,
    Row,
    Col,
    Button,
    Table
} from 'reactstrap'
import { useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'
import dashboardService from '../services/dashboardService'
import DashboardWidget from '../components/DashboardWidget'
import { notify } from 'react-notify-toast';
import Loading from '../components/Loading'

const Dashboard = (props) => {

    const history = useHistory()
    const user = useSelector(state => state.user)
    const currency = useSelector(state => state.currency)
    const [transactions, setTransaction] = useState({});
    const [dashboardData, setDashboardData] = useState({});
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        if(!user) {
            history.push('/login')
        } else {
            dashboardService.getTransactions(user)
                .then(res => {
                    if(res.data.success) {
                        setTransaction(res.data.data)
                    }
                })
                .catch(function (error) {
                    // handle error
                })
                .finally(function () {
                    setLoading(false)
                });

            dashboardService.getWidgetData(user)
                .then(res => {
                    if(res.data.success) {
                        setDashboardData(res.data.data)
                    }
                })
                .catch(function (error) {
                    // handle error
                })
                .finally(function () {
                    setLoading(false)
                });
        }
        notify.hide()

    },[])

    const handleRowClick = e => {
        let hiddenRow = e.currentTarget.nextSibling.children[0].children[0].classList
        if (hiddenRow[1] === 'collapse') {
            hiddenRow.remove('collapse')
        } else {
            hiddenRow.add('collapse')
        }
        
    }

    let parser = new DOMParser();

    if(loading) {
        return <Loading />
    }

    return (
        <div style={{minHeight: '100vh'}}>
            <Row>
                <Col sm="6" md="3">
                    <DashboardWidget 
                        title="Number of products"
                        data={dashboardData.number_of_products}
                        className="bg-info text-white mt-4 mb-4"
                    />
                </Col>
                <Col sm="6" md="3">
                    <DashboardWidget 
                        title="Number of sold products"
                        data={dashboardData.number_of_sold_products}
                        className="bg-secondary text-white mt-4 mb-4"
                    />
                </Col>
                <Col sm="6" md="3">
                    <DashboardWidget 
                        title="Number of transactions"
                        data={dashboardData.number_of_transactions}
                        className="bg-success text-white mt-4 mb-4"
                    />
                </Col>
                <Col sm="6" md="3">
                    {currency === 'euro' ? 
                    <DashboardWidget 
                        title="Total income"
                        data={Number(dashboardData.total_income_euro).toFixed(2) + ' ' + parser.parseFromString('&euro;', 'text/html').body.innerHTML}
                        className="bg-primary text-white mt-4 mb-4"
                    />
                    :
                    <DashboardWidget 
                        title="Total income"
                        data={Number(dashboardData.total_income_dollar).toFixed(2) + ' ' + parser.parseFromString('&#36;', 'text/html').body.innerHTML}
                        className="bg-primary text-white mt-4 mb-4"
                    />
                    }
                </Col>
            </Row>
            <Row>
                <Col className="table-responsive">
                    <table style={{backgroundColor: 'white', width: '100%'}} className='table table-hover table-striped text-center'>
                        <thead className="thead-dark">
                            <tr>
                                <th>Transaction id</th>
                                <th>Customer name</th>
                                <th>Customer address</th>
                                <th>Customer phone</th>
                                <th>Total price</th>
                                <th>Date of order</th>
                            </tr>
                        </thead>
                        
                        {Object.values(transactions).map(data => {
                            
                            let euroSymbol = parser.parseFromString('&euro;', 'text/html').body.innerHTML;
                            let dollarSymbol = parser.parseFromString('&#36;', 'text/html').body.innerHTML;

                            let prices = {}
                            if(currency === 'euro') {
                                prices['total_price'] = data.transaction.products_total_price_euro,
                                prices['delivery_costs'] = data.transaction.delivery_cost_euro,
                                prices['symbol'] = euroSymbol
                            } else {
                                prices['total_price'] = data.transaction.products_total_price_dollar,
                                prices['delivery_costs'] = data.transaction.delivery_cost_dollar,
                                prices['symbol'] = dollarSymbol
                            }

                            let key = data.transaction.id

                            return(
                                <tbody key={key}>
                                    <tr onClick={handleRowClick} style={{cursor: 'pointer'}} >
                                        <td>{data.transaction.id}</td>
                                        <td>{data.transaction.customer_name}</td>
                                        <td>{data.transaction.customer_address}</td>
                                        <td>{data.transaction.customer_phone}</td>
                                        {currency === 'euro' ? 
                                        <td>{(Number(data.transaction.products_total_price_euro) + Number(data.transaction.delivery_cost_euro)).toFixed(2)} &euro;</td>
                                        :
                                        <td>{(Number(data.transaction.products_total_price_dollar) + Number(data.transaction.delivery_cost_dollar)).toFixed(2)} &#36;</td>
                                        }
                                        <td>{data.transaction.created_at}</td>
                                    </tr>
                                    <tr key={key + 100000}>
                                        <td colSpan="12" className="hiddenRow">
                                            <div className="accordian-body collapse" style={{padding: '30px'}}>
                                                <table className="table">
                                                    <thead className="thead-dark">
                                                        <tr>
                                                            <th>Image</th>
                                                            <th>Product</th>
                                                            <th>Price</th>
                                                            <th>Quantity</th>
                                                            <th>Item total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    {Object.values(data.products).map(data => {

                                                        return(
                                                            <tr key={data.image.img_path}>
                                                                <td width="200">
                                                                    <img src={data.image.img_path} width="100%"/>
                                                                </td>
                                                                <td>{data.product.title}</td>
                                                                {currency === 'euro' ?
                                                                <td>{data.product.price.euro} {euroSymbol}</td>
                                                                :
                                                                <td>{data.product.price.dollar} {dollarSymbol}</td>
                                                                }
                                                                <td>{data.quantity}</td>
                                                                {currency === 'euro' ?
                                                                <td>{(data.product.price.euro * data.quantity).toFixed(2)} {euroSymbol}</td>
                                                                :
                                                                <td>{(data.product.price.dollar * data.quantity).toFixed(2)} {dollarSymbol}</td>
                                                                }
                                                            </tr>
                                                        )
                                                    })}
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>Item(s) total:</td>
                                                        <td><b>{prices.total_price} {prices.symbol}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>Delivery costs:</td>
                                                        <td><b>{prices.delivery_costs} {prices.symbol}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td><b>Subtotal:</b></td>
                                                        <td><b>{(Number(prices.delivery_costs) + Number(prices.total_price)).toFixed(2)} {prices.symbol}</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            )
                        })}
                    </table>
                </Col>
            </Row>
        </div>
    )
}

export default Dashboard;