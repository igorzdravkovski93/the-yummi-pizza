import React, { useState, useEffect } from 'react';
import {
    Form,
    FormGroup,
    Label,
    Input,
    Row,
    Col,
    Button
} from 'reactstrap'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { login } from '../store/actions/user'
import userService from '../services/userService'
import { notify } from 'react-notify-toast';
import Loading from '../components/Loading'

const Login = (props) => {

    const [loginData, setLoginData] = useState({email: '', password: ''});
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState('')
    const history = useHistory()
    const user = useSelector(state => state.user)
    const dispatch = useDispatch()

    useEffect(() => {
        if(user) {
            history.push('/dashboard')
        }

        notify.show(
            <div>Enter email: <span className="text-danger">admin@example.com</span> and password: <span className="text-danger">admin123</span></div>, "warning", 15000);
        setLoading(false);
    },[])

    const handleInputChange = event => {
        let inputId = event.currentTarget.id
        let inputValue = event.currentTarget.value

        let newState = {...loginData};
        newState[inputId] = inputValue;
        setLoginData(newState)
    }

    const handleLogin = () => {
        if(loginData.email.length > 0 && loginData.password.length > 0) {
            userService.login(loginData).
                then(res => {
                    if(res.data.error) {
                        setError(res.data.msg)
                    } else {
                        dispatch(login(loginData));
                        history.push('/dashboard')
                    }
                })
        } else {
            setError('All fields are required!')
        }
    }

    if(loading) {
        return <Loading />
    }

    return (
        <Row style={styles.loginView} className="d-flex flex-column justify-content-center">
            <Col style={styles.loginContainer} xs={{size:8, offset: 2}} sm={{size:6, offset: 3}} md={{size:4, offset: 4}} >
                <div style={styles.loginForm} className="d-flex flex-column justify-content-center align-items-center">
                    <h1 className="text-center mb-4">Login</h1>
                    {error.length > 0 ?
                    <p className="text-danger">{error}</p>
                    : null}
                    <Form>
                        <FormGroup>
                            <Label for="email">Email</Label>
                            <Input type="email" id="email" placeholder="Email" onChange={handleInputChange} value={loginData.email} onFocus={() => setError('')}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="password">Password</Label>
                            <Input type="password" id="password" placeholder="Password" onChange={handleInputChange} value={loginData.password} onFocus={() => setError('')}/>
                        </FormGroup>
                        <Button color="outline-warning" onClick={handleLogin} block>Login</Button>
                    </Form>
                </div>
            </Col>
        </Row>
    )
}

const styles = {
    loginView: {
        height: '100vh',
    },
    loginContainer: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        borderRadius: '20px',
        padding: '10px',
    },
    loginForm: {
        backgroundColor: 'white',
        padding: '30px',
        borderRadius: '20px',
        border: '4px solid rgb(255, 193, 7)',
        height: '100%'
    }
}

export default Login;