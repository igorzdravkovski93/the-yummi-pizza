import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import {
    Container, 
    Col,
    Row
} from 'reactstrap'
import { notify } from 'react-notify-toast';

import Slider from "react-slick";
import PromoCard from '../components/PizzaPromotionCard'
import productsService from '../services/productsService'
import Loading from '../components/Loading'

const Home = () => {
    
    const [topProducts, setTopProducts] = useState([]);
    const [newestProducts, setNewestProducts] = useState([]);
    const [loading, setLoading] = useState(true)

    const history = useHistory();

    useEffect(() => {
        productsService.getTopProducts()
            .then(response => {
                setTopProducts(response.data.data)
            })
            .catch(err => {})

        productsService.getNewestProducts()
            .then(response => {
                setNewestProducts(response.data.data)
            })
            .catch(err => {})

            notify.show(
                <div>Buy pizza and go to the <a href='/login' className="text-danger">ADMIN DASHBOARD</a></div>, 
                "warning", 7000);
    }, [])

    useEffect(() => {
        if(topProducts.length > 0 && newestProducts.length > 0) {
            setLoading(false)
        }
    }, [topProducts, newestProducts])

    if(loading) {
        return <Loading />
    }
    
    return (
        <Container style={styles.view}>
            <Row>
                <Col>
                    <div style={styles.title} className="d-flex justify-content-center align-items-center">
                        <h1>Welcome to THE YUMMI PIZZA</h1>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col style={styles.sectionTitle}>
                    <h3>Top products</h3>
                </Col>
            </Row>
            <Row style={{padding: '10px'}}>
                <Col md="12">
                    <Slider slidesPerRow={2} infinite={true}>
                        {topProducts.map((item) => {

                            return (
                                <PromoCard 
                                    key={item.product.id}
                                    title={item.product.title}
                                    className="d-flex justify-content-center"
                                    cardStyle={{
                                        backgroundImage: `url(${item.image})`,
                                        display: 'inline-block',
                                        height: '30vh',
                                        width: '40%',
                                        margin: '5%',
                                        backgroundPosition: 'center',
                                        backgroundSize: 'cover',
                                        backgroundRepeat: 'no-repeat',
                                        color: 'black',
                                        paddingTop: '3%',
                                        borderRadius: '20px',
                                        boxShadow: '5px 5px 5px black',
                                        cursor: 'pointer'
                                    }}
                                    titleStyle={{
                                        textAlign: 'center',
                                        height: '50%',
                                        border: '1px solid black',
                                        backgroundColor: 'rgba(255, 193, 7, 0.7)',
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }} 
                                    onClick={() => history.push(`/menu/${item.product.id}`)}/>
                            )
                        })}
                    </Slider>
                </Col>
            </Row>
            <Row>
                <Col style={styles.sectionTitle}>
                    <h3>Newest products</h3>
                </Col>
            </Row>
            <Row style={{padding: '10px'}}>
                <Col md="12">
                    <Slider slidesPerRow={2} infinite={true}>
                        {newestProducts.map((item) => {

                            return (
                                <PromoCard 
                                    key={item.product.id}
                                    title={item.product.title}
                                    className="d-flex justify-content-center"
                                    cardStyle={{
                                        backgroundImage: `url(${item.image})`,
                                        display: 'inline-block',
                                        height: '30vh',
                                        width: '40%',
                                        margin: '5%',
                                        backgroundPosition: 'center',
                                        backgroundSize: 'cover',
                                        backgroundRepeat: 'no-repeat',
                                        color: 'black',
                                        paddingTop: '3%',
                                        borderRadius: '20px',
                                        boxShadow: '5px 5px 5px black',
                                        cursor: 'pointer'
                                    }}
                                    titleStyle={{
                                        textAlign: 'center',
                                        height: '50%',
                                        border: '1px solid black',
                                        backgroundColor: 'rgba(255, 193, 7, 0.7)',
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }} 
                                    onClick={() => history.push(`/menu/${item.product.id}`)}/>
                            )
                        })}
                    </Slider>
                </Col>
            </Row>
        </Container>
    );
}

const styles = {
    view: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        marginTop: '2%',
        marginBottom: '2%',
        borderRadius: '20px',
    },
    title: {
        color: 'white',
        height: '20vh',
        margin: '3% 0',
        borderRadius: '20px',
        textAlign: 'center',
    },
    sectionTitle: {
        color: 'white',
        borderBottom: '3px solid rgb(255, 193, 7)',
        margin: '2%',
    }
}

export default Home;
