import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from "react-router-dom";
import {
    Container, 
    Col,
    Row,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Button
} from 'reactstrap'
import swal from 'sweetalert2';

import productsService from '../services/productsService'
import { addProduct } from '../store/actions/productToCard'
import Loading from '../components/Loading'


const MenuItem = (props) => {

    const [menuItem, setMenuItem] = useState([]);
    const [inCart, setInCart] = useState([]);
    const [loading, setLoading] = useState(true)

    const currency = useSelector(state => state.currency)
    const shoppingCart = useSelector(state => state.shoppingCart)
    const history = useHistory();
    const dispatch = useDispatch();
    

    useEffect(() => {
        Object.values(shoppingCart).map( item => {
            setInCart(ids => [...ids, item.product.id])
        })  
    }, [shoppingCart])

    useEffect(() => {
        
        let itemId = window.location.pathname.split('/').slice(-1)[0]

        productsService.getCurrentProduct(itemId)
        .then((response) => {
            setMenuItem(response.data.data)
        })
        .catch(function (error) {
            // handle error
        })
        .finally(function () {
            setLoading(false)
        });

    }, [])

    const addProductToCart = data => {
        let boolean = false
        let dataToStore = {...data, quantity: 1};
        inCart.map(id => {
            if(id === data.product.id) {
                boolean = true;
            }
        })
        if(!boolean) {
            dispatch(addProduct(dataToStore))
            swal.fire({
                title: 'Success',
                text: "Product added to your cart!",
                type: 'success',
              })
        } else {
            swal.fire({
                title: 'Info',
                text: "Product is already in your cart!",
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Go to cart!',
                cancelButtonText: 'Order more items!'
              }).then((result) => {
                if (result.value) {
                    history.push('/checkout')
                }
              })
        }
    }

    if(loading) {
        return <Loading />
    }

    return (
        <Container>
            <Row>
                <Col md="1" sm="2" xs="3">
                    <Button block color="secondary" style={styles.backButton} onClick={() => 
                        history.goBack()
                    }>
                        <i className="fas fa-angle-left"></i>
                    </Button>
                </Col>
            </Row>
            
            
                {menuItem.map(item => {
                    if(currency === 'euro') {
                        return(
                            <Row className="no-gutters" style={styles.contentContainer} key={item.product.id} >
                                <Col sm={{size: 8, offset: 2}} xs={{size: 10, offset: 1}}>
                                    <img src={item.image} width="100%" height="100%"/>
                                </Col>
                                <Col md="12">
                                    <Card>
                                        <CardHeader className="d-flex justify-content-between"
                                        style={styles.cardHeader}>
                                            <div>{item.product.title}</div>
                                            <div>{item.price_euro} &euro;</div>
                                        </CardHeader>
                                        <CardBody>
                                            {item.product.description}
                                        </CardBody>
                                        <CardFooter className="text-center">
                                            <Button color="outline-danger" onClick={() => addProductToCart(item)}>ORDER NOW</Button>
                                        </CardFooter>
                                    </Card>
                                </Col>
                            </Row>
                        )
                    } else {
                        return(
                            <Row className="no-gutters" style={styles.contentContainer} key={item.product.id} >
                                <Col sm={{size: 8, offset: 2}} xs={{size: 10, offset: 1}}>
                                    <img src={item.image} width="100%" height="100%"/>
                                </Col>
                                <Col md="12">
                                    <Card key={item.product.id}>
                                        <CardHeader className="d-flex justify-content-between"
                                        style={styles.cardHeader}>
                                            <div>{item.product.title}</div>
                                            <div>{item.price_dollar} &#36;</div>
                                        </CardHeader>
                                        <CardBody>
                                            {item.product.description}
                                        </CardBody>
                                        <CardFooter style={styles.cardFooter} className="text-center">
                                            <Button color="outline-danger" onClick={() => addProductToCart(item)}>ORDER NOW</Button>
                                        </CardFooter>
                                    </Card>
                                </Col>
                            </Row>
                        )
                    }
                })}
        </Container>
    );
}

    const styles = {
        contentContainer: {
            margin: '5%',
            backgroundColor: 'rgba(0, 0, 0, 0.9)',
            borderRadius: '10px',  
            border: '2px solid rgb(255, 193, 7)'
        },
        cardHeader: {
            backgroundColor: 'white',
            borderBottom: '2px solid black',
            fontWeight: 'bold'
        },
        cardFooter: {
            backgroundColor: 'white',
            border: 0
        },
        backButton: {
            marginTop: '20%',
            color: 'black',
            background: 'rgb(255, 193, 7)'
        }
    }


export default MenuItem;
