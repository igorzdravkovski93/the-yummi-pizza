import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from "react-router-dom";
import {
    Container, 
    Col,
    Row,
    Button
} from 'reactstrap'
import swal from 'sweetalert2';

import PizzaCard from '../components/PizzaCard'
import productsService from '../services/productsService'
import { addProduct } from '../store/actions/productToCard'
import Loading from '../components/Loading'

const Menu = () => {

    const [menuItems, setMenuItems] = useState([]);
    const [inCart, setInCart] = useState([]);
    const [loading, setLoading] = useState(true)

    const currency = useSelector(state => state.currency)
    const shoppingCart = useSelector(state => state.shoppingCart)
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        productsService.getProducts()
        .then((response) => {
            setMenuItems(response.data.data)
        })
        .catch(function (error) {
            // handle error
        })
        .finally(function () {
            setLoading(false)
        });
        
        
    }, [])

    useEffect(() => {
        Object.values(shoppingCart).map( item => {
            setInCart(ids => [...ids, item.product.id])
        })  
    }, [shoppingCart])

    const addProductToCart = data => {
        let boolean = false
        let dataToStore = {...data, quantity: 1};
        inCart.map(id => {
            if(id === data.product.id) {
                boolean = true;
            }
        })
        if(!boolean) {
            dispatch(addProduct(dataToStore))
            swal.fire({
                title: 'Success',
                text: "Product added to your cart!",
                type: 'success',
              })
        } else {
            swal.fire({
                title: 'Info',
                text: "Product is already in your cart!",
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Go to cart!',
                cancelButtonText: 'Order more items!'
              }).then((result) => {
                if (result.value) {
                    history.push('/checkout')
                }
              })

        }
    }
    
    if(loading) {
        return <Loading />
    }

    return (
        <Container style={styles.view}>
            <Row>
                <Col style={styles.title}>
                    <h3>Menu</h3>
                </Col>
            </Row>
            <Row className="no-gutters">
                {menuItems.map(item => {
                    return(
                        <Col md="4" key={item.product.id}>
                            {currency === 'euro' ? 
                            <PizzaCard 
                                img={item.image}
                                title={item.product.title}
                                description={item.product.description}
                                price={item.price_euro}
                                currencyIcon="&euro;"
                                refStyle={{color: 'red', cursor: 'pointer'}}
                                onItemClick={() => history.push(`/menu/${item.product.id}`)}
                                onAddToCart={() => addProductToCart(item)}/>
                                
                            :
                            <PizzaCard 
                                img={item.image}
                                title={item.product.title}
                                description={item.product.description}
                                price={item.price_dollar}
                                currencyIcon="&#36;"
                                refStyle={{color: 'red', cursor: 'pointer'}}
                                onItemClick={() => history.push(`/menu/${item.product.id}`)}
                                onAddToCart={() => addProductToCart(item)}/>
                            }
                        </Col>
                    )
                })}
            </Row>
        </Container>
    );
}

    const styles = {
        titleContainer: {
            margin: '3%'
        },
        title: {
            color: 'white',
            backgroundColor: 'rgba(0, 0, 0, 0.7)',
            borderBottom: '3px solid rgb(255, 193, 7)',
            margin: '2% 0'
        }
    }


export default Menu;
