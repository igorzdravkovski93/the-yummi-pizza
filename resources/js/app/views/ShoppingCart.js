import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from "react-router-dom";
import {
    Container,
    Row,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    Button
} from 'reactstrap'
import swal from 'sweetalert2';

import ShoppingCartItem from '../components/ShoppingCartItem'
import { modifyProduct, emptyCard } from '../store/actions/productToCard'
import paymentService from '../services/paymentService'
import Loading from '../components/Loading'

const ShoppingCart = (props) => {

    const history = useHistory()
    const dispatch = useDispatch();
    const currency = useSelector(state => state.currency)
    const shoppingCart = useSelector(state => state.shoppingCart)
    const [checkoutItems, setCheckoutItems] = useState(shoppingCart)
    const [errorMessages, setErrorMessages] = useState({})
    const [loading, setLoading] = useState(true)
    const [customerInfo, setCustomerInfo] = useState({
        name: '',
        address: '',
        phone: '',
        products_price: {
            euro: 0, dollar: 0
        },
        delivery_costs: {
            euro: 0, dollar: 0
        },
        products: [] 
    })

    const handleQuantityChange = (event, data) => {
        let inputValue = event.currentTarget.value
        if(inputValue === '') {
            inputValue = 1
        }
        let oldState = [...checkoutItems];
        let a = Object.entries(oldState).filter(obj => {
            if(obj[1].product.id === data.product.id) {
                return obj[0]
            }
        })
        oldState[a[0][0]].quantity = Number(inputValue)
        setCheckoutItems(oldState)
        dispatch(modifyProduct(oldState))
    }

    const handleDelete = (id) => {
        swal.fire({
            title: 'Are you sure?',
            text: "Remove this product from your shopping cart?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, remove it!'
          }).then((result) => {
            if (result.value) {
                let oldItems = [...checkoutItems];
                let newItems = Object.values(oldItems).filter(obj => {
                    return obj.product.id !== id
                })
                setCheckoutItems(newItems)
                dispatch(modifyProduct(newItems))

                swal.fire({
                    title: 'Deleted!',
                    text: 'Your product has been removed from your shopping cart.',
                    type: 'success'
                })
            }
          })
    }

    const handlePayment = (data) => {
        setErrorMessages({})
        swal.showLoading()
        paymentService.processPayment(data)
            .then(res => {
                swal.close()
                if(res.data.error) {
                    let errors = {}
                    Object.entries(res.data.messages).map(msg => {
                        errors[msg[0]] = msg[1][0]
                    })
                    setErrorMessages(errors)
                }
                if(res.data.success) {
                    let date = new Date()
                    swal.fire({
                        type: 'success',
                        title: res.data.messageTitle,
                        text: res.data.message,
                        footer: `THE YUMMI PIZZA &copy; ${date.getFullYear()}`
                    }).then((result) => {
                        if (result.value) {
                            dispatch(emptyCard());
                            history.push('/menu')
                        }
                      })
                }
            })
            .catch(error => {
                
            })
    }

    const handleInputChange = event => {
        let inputKey = event.currentTarget.id
        let inputValue = event.currentTarget.value
        let newInfo = {...customerInfo}

        newInfo[inputKey] = inputValue;
        
        setCustomerInfo(newInfo)
    }

    useEffect(() => {
        let totalEuro = 0
        let totalDollar = 0
        let deliveryCostsEuro = 0
        let deliveryCostsDollar = 0
        let newInfo = {...customerInfo}
        let products = []
        
        Object.values(checkoutItems).map(item => {
            totalEuro += Number((item.price_euro * item.quantity).toFixed(2))
            totalDollar += Number((item.price_dollar * item.quantity).toFixed(2))
            products.push({product_id: item.product.id, quantity: item.quantity})
        })

        if(totalDollar < 23) {
            deliveryCostsDollar = (totalDollar / 5).toFixed(2)
        }

        if(totalEuro < 21) {
            deliveryCostsEuro = (totalEuro / 5).toFixed(2)
        }

        newInfo.products_price = {
            euro: totalEuro.toFixed(2),
            dollar: totalDollar.toFixed(2)
        }
        newInfo.delivery_costs = {
            euro: deliveryCostsEuro,
            dollar: deliveryCostsDollar
        }
        newInfo.products = products

        setCustomerInfo(newInfo)
        setLoading(false)
    }, [checkoutItems])


    const finalPrices = (currency) => {
        if(currency === 'euro') {
            return (
                <>
                <FormGroup>
                    <FormGroup className="d-flex justify-content-between border-bottom ">
                        <Label>Item(s) total: </Label>
                        <p className="font-weight-bold">{customerInfo.products_price.euro} &euro;</p>
                    </FormGroup >
                    <FormGroup className="d-flex justify-content-between border-bottom ">
                        <Label>Delivery: </Label>
                        <p className="font-weight-bold">{customerInfo.delivery_costs.euro} &euro;</p>
                    </FormGroup>
                </FormGroup>
                <FormGroup>
                    <Col className="d-flex justify-content-between" style={styles.subtotal}>
                        <Label className="text-uppercase font-weight-bold" style={{marginBottom: 0}}>Subtotal: </Label>
                        <p className="font-weight-bold" style={{marginBottom: 0}}>{(Number(customerInfo.delivery_costs.euro) + Number(customerInfo.products_price.euro)).toFixed(2)} &euro;</p>
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col>
                        <p className="text-secondary small">* Delivery is free for orders above 21 &euro;</p>
                    </Col>
                </FormGroup>
                </>
            )
        } else {
            return (
                <>
                <FormGroup>
                    <FormGroup className="d-flex justify-content-between border-bottom ">
                        <Label>Item(s) total: </Label>
                        <p className="font-weight-bold">{customerInfo.products_price.dollar} &#36;</p>
                    </FormGroup >
                    <FormGroup className="d-flex justify-content-between border-bottom ">
                        <Label>Delivery: </Label>
                        <p className="font-weight-bold">{customerInfo.delivery_costs.dollar} &#36;</p>
                    </FormGroup>
                </FormGroup>
                <FormGroup>
                    <Col className="d-flex justify-content-between" style={styles.subtotal}>
                        <Label className="text-uppercase font-weight-bold" style={{marginBottom: 0}}>Subtotal: </Label>
                        <p className="font-weight-bold" style={{marginBottom: 0}}>{(Number(customerInfo.delivery_costs.dollar) + Number(customerInfo.products_price.dollar)).toFixed(2)} &#36;</p>
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Col>
                        <p className="text-secondary small">* Delivery is free for orders above 23 &#36;</p>
                    </Col>
                </FormGroup>
                </>
            )
        }
    }

    if(loading) {
        return <Loading />
    }

    return (
        <Container>
            {shoppingCart.length > 0 ? 
            <Row>
                {currency === 'euro' ? 
                <Col xs="12" md="8" style={styles.listContainer}>
                    <h2 className="text-white text-center mt-4">My Shopping Cart</h2>
                {Object.values(checkoutItems).map(item => {
                    return (
                        
                        <ShoppingCartItem 
                            key={item.product.id * Math.floor(Math.random() * 10000)}
                            image={item.image}
                            title={item.product.title}
                            listItemContainer={styles.listItemContainer}
                            price={item.price_euro}
                            quantity={item.quantity}
                            total={(item.price_euro * item.quantity).toFixed(2)}
                            onChange={(event) => handleQuantityChange(event, item)}
                            onDelete={() => handleDelete(item.product.id)}
                            currency="&euro;"
                        />
                        
                    )
                }) }
                
                </Col>
                : 
                <Col xs="12" md="8" style={styles.listContainer}>
                    <h2 className="text-white text-center mt-4">My Shopping Cart</h2>
                {Object.values(checkoutItems).map(item => {

                    return (
                        
                        <ShoppingCartItem 
                            key={item.product.id * Math.floor(Math.random() * 100)}
                            image={item.image}
                            title={item.product.title}
                            listItemContainer={styles.listItemContainer}
                            price={item.price_dollar}
                            quantity={item.quantity}
                            total={(item.price_dollar * item.quantity).toFixed(2)}
                            onChange={(event) => handleQuantityChange(event, item)}
                            onDelete={() => handleDelete(item.product.id)}
                            currency="&#36;"
                        />
                        
                    )
                }) }
                
                </Col>
                }
                <Col xs="12" md="4" style={styles.listContainer}>
                    <h2 className="text-white text-center mt-4">Billing Details</h2>
                    <div style={styles.listItemContainer}>
                        <Form style={styles.form}>
                            <FormGroup>
                                <Label for="name">Full name</Label>
                                <Input type="text" id="name" placeholder="Enter your name" onChange={handleInputChange} value={customerInfo.name}/>
                                {errorMessages.name ? 
                                <p className="text-danger small">{errorMessages.name}</p>
                                : null}
                            </FormGroup>
                            <FormGroup>
                                <Label for="address">Address</Label>
                                <Input type="text" id="address" placeholder="Enter your address" onChange={handleInputChange} value={customerInfo.address}/>
                                {errorMessages.address ? 
                                <p className="text-danger small">{errorMessages.address}</p>
                                : null}
                            </FormGroup>
                            <FormGroup>
                                <Label for="Phone">Phone</Label>
                                <Input type="text" id="phone" placeholder="Enter your phone" onChange={handleInputChange} value={customerInfo.phone}/>
                                {errorMessages.phone     ? 
                                <p className="text-danger small">{errorMessages.phone}</p>
                                : null}
                            </FormGroup>
                            {finalPrices(currency)}
                            <FormGroup>
                                <Button block color="outline-warning" onClick={() => handlePayment(customerInfo)}>PAY NOW</Button>
                            </FormGroup>
                        </Form>
                    </div>
                </Col>
            </Row>
            :
            <Row>
                <Col md={{size:6, offset:3}} className="mt-3 mb-3" style={styles.emptyCardContainer}>
                    <div style={styles.emptyCart} className="d-flex flex-column justify-content-center align-items-center mt-3 mb-3">
                        <img className="mt-2" src="/images/empty_card.png" height="50%" width="90%"/>
                        <h2 className="mt-4 text-center">Your shopping cart is empty :(</h2>
                        <Button className="mt-4" color="outline-danger" onClick={() => history.push('/menu')}>ORDER PIZZA NOW</Button>
                    </div>
                </Col>
            </Row>    
            }
        </Container>
    );
}

const styles = {
    listContainer: {
        background: 'rgba(0, 0, 0, 0.7)',
        margin: '2% 0'
    },
    listItemContainer: {
        background: "white",
        padding: '10px',
        margin: '40px 0',
        border: '3px solid rgb(255, 193, 7)'
    },
    form: {
        padding: '10px'
    },
    emptyCardContainer: {
        background: 'rgba(0, 0, 0, 0.7)',
        borderRadius: '20px'
    },
    emptyCart: {
        height: '80vh',
        background: 'white',
        border: '4px solid rgb(255, 193, 7)',
        marginTop: '5%',
        borderRadius: '20px',
    },
    subtotal: {
        border: '2px solid black',
        borderRadius: '10px',
        padding: '10px 10px'
    }
}

export default ShoppingCart;