import React from 'react';
import {
    Card,
    CardBody
} from 'reactstrap'

const PromotionCard = (props) => {
    return (
        <Card style={props.cardStyle} onClick={props.onClick}>
            <CardBody style={props.cardBodyStyle}>
                <img src={props.src} />
            </CardBody>
            <div style={props.titleStyle}>
                <p>{props.title}</p>
            </div>
        </Card>
    );
};

export default PromotionCard;