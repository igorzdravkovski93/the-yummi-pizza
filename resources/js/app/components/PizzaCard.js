import React, { useEffect } from 'react';

import {  
    Card, 
    Button, 
    CardHeader, 
    CardFooter, 
    CardBody,
    CardTitle, 
    CardText
} from 'reactstrap'


const PizzaCard = (props) => {
    
    useEffect(() => {
        const cards = document.getElementsByClassName('pizzaCard');
        const heights = []
        for (let card of cards) {
            heights.push(card.clientHeight);
        }
        let max = 0
        for (let i = 0; i < heights.length; i++) {
            if(heights[i] > max) {
                max = heights[i]
            }
        }
        for (let card of cards) {
            card.style.height = `${max}px`
        }
        
    })
    
    return (
        <Card style={styles.card}>
            <img width="100%" alt="pizza" src={props.img} style={styles.image}/>
            <CardBody  className="pizzaCard">
                <CardTitle style={styles.title}>{props.title}</CardTitle>
                <CardText>{props.description.substr(0,60)}...  <span style={props.refStyle} onClick={props.onItemClick} >see more</span></CardText>
            </CardBody>
            <CardFooter>
                <Button onClick={props.onAddToCart} color="outline-danger">Order now</Button>
                <p style={styles.price}>{props.price} {props.currencyIcon}</p>
            </CardFooter>
        </Card>
    );
}

const styles = {
    card: {
        margin: '5% 3%',
        border: '2px solid rgb(255, 193, 7)',
        background: 'rgba(255, 252, 235, 0.9)',
        borderRadius: '20px',
    },
    image: {
        borderRadius: '20px 20px 0 0',
    },
    title: {
        fontWeight: 'bold'
    },
    description: {
        paddingRight: '10px'
    },
    button: {
        position: 'absolute',
        bottom: 5,
        right: 20
    },
    price: {
        position: 'absolute',
        bottom: 5,
        right: 20,
        fontWeight: 'bold',
    },
}

export default PizzaCard;