import React from 'react'
import {
    Card,
    CardTitle,
    CardHeader
} from 'reactstrap'

const DashboardWidget = (props) => {
    return (
        <Card body className={props.className} style={styles.widget}>
            <CardHeader className="text-center">{props.title}</CardHeader>
            <CardTitle className='d-flex justify-content-center align-items-center mt-2'>
                <h1 className="font-weight-bold">{props.data}</h1>
            </CardTitle>
        </Card>
    );
}

const styles = {
    widget: {
        borderRadius: '20px',
        boxShadow: '5px 5px 10px black'
    }
}

export default DashboardWidget;