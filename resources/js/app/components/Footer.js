import React from 'react'

const Footer = props => {
    return (
        <div className="text-center bg-warning mt-2" style={{height: '40px', padding: '8px 0'}}>THE YUMMI PIZZA &copy; 2020</div>
    )
}

export default Footer;