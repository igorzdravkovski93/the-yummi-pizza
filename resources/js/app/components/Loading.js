import React from 'react';
import { Spinner } from 'reactstrap'; 

const Loading = props => {
    return (
        <div style={{height: '100vh'}} className="d-flex justify-content-center align-items-center">
            <Spinner size="lg" color="warning" />
        </div>
    )
}

export default Loading;