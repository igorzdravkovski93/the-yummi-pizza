import React, { useState } from 'react';
import { 
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  NavLink,
  Input,
  Badge
} from 'reactstrap';
import { euro, dollar } from '../store/actions/currencyChange'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from "react-router-dom";
import { logout } from '../store/actions/user'

const Header = () => {
  
    const [isOpen, setIsOpen] = useState(false);

    const dispatch = useDispatch()
    const history = useHistory()
   
    const currency = useSelector(state => state.currency)
    const shoppingCart = useSelector(state => state.shoppingCart)

    const toggle = () => setIsOpen(!isOpen);

    const handleCurrencyChange = (e) => {
      let value = e.currentTarget.value
      if(value === 'euro') {
        dispatch(euro())
      } else {
        dispatch(dollar())
      }
    }

    const handleLogout = () => {
        dispatch(logout())
        history.push('/login')
    }

    return (
      <>
        <Navbar color="warning" light expand="md">
          <NavbarBrand href="/">THE YUMMI PIZZA</NavbarBrand>
          { history.location.pathname !== '/login' && history.location.pathname !== '/dashboard' ?
          <>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar className="d-md-flex justify-content-md-between">
            <div className="d-md-flex">
            <NavLink style={styles.link} href="/">Home</NavLink>
            <NavLink style={styles.link} href="/menu">Menu</NavLink>
            </div>
            <div className="d-md-flex">
            <NavLink style={styles.link} className="d-flex flex-row" href="/checkout"><i className="fas fa-shopping-cart"></i><Badge color="danger" pill>{shoppingCart.length}</Badge></NavLink>
            <Input 
              type="select" 
              bsSize="sm"
              defaultValue={currency}
              onChange={handleCurrencyChange}>
                <option value='euro'>&euro;</option>
                <option value='dollar'>&#36;</option>
            </Input>
            </div>
          </Collapse>
          </>
          : null }
          {history.location.pathname === '/dashboard' ?
          <>
          <NavbarToggler onClick={toggle} />
          <div style={{width: '70%'}}></div>
          <Collapse isOpen={isOpen} navbar className="d-md-flex
          justify-content-md-end">
            <Input 
              type="select" 
              bsSize="sm"
              defaultValue={currency}
              onChange={handleCurrencyChange}>
                <option value='euro'>&euro;</option>
                <option value='dollar'>&#36;</option>
            </Input>
            <NavLink style={styles.link} onClick={handleLogout}>Logout</NavLink>
          </Collapse>
          </>
          : null }
      </Navbar>
      </>
    );
}

const styles = {
  link: {
    color: 'black',
    cursor: 'pointer'
  },
}

export default Header;