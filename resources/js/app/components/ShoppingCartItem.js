import React from 'react';
import {
    Row,
    Col,
    Input,
    Button
} from 'reactstrap'

const ShoppingCartItem = (props) => {
    return (
        <Row style={props.listItemContainer}>
            <Col xs="12" md="4">
                <div style={props.productImage}>
                    <img src={props.image} width="100%"/>
                </div>
            </Col>
            <Col xs="12" md="8">
                <div style={props.productData} className="d-flex flex-column">
                    <h5 className="font-weight-bold">{props.title}</h5>
                    <div className="d-flex flex-row">
                        <Col xs="8" style={{marginLeft: '-15px'}}>
                            <p>Quantity: </p>
                        </Col>
                        <Col xs="4" className="d-flex flex-row">
                            <Input type="number"  value={props.quantity} onChange={props.onChange} bsSize="sm" min="1"></Input>
                        </Col>
                        
                    </div>
                    <p >Price: <span className="font-weight-bold">{props.price} {props.currency}</span></p>
                    <p>Total: <span className="font-weight-bold">{props.total} {props.currency}</span></p>
                    <div className="text-right">
                        <Button size="sm" color="outline-danger" onClick={props.onDelete}>Remove from cart</Button>
                    </div>
                </div>
            </Col>
        </Row>
    );
}

export default ShoppingCartItem;