module.exports = {
    entry: ['babel-polyfill', './resources/js/app/App.js'],
    output: {
      path: __dirname + '/build',
      filename: 'bundle.js'
    },
    module: {
      rules: [
        {
          test: /\.js$/, // a regular expression that catches .js files
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
      ]
    },
    devServer: {
      port: 3000, // most common port
      contentBase: './build',
      inline: true
    }
  }